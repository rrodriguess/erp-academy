package br.com.berserker.web.service;

import br.com.berserker.core.entities.Cidade;
import br.com.berserker.core.repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CidadeService {

    @Autowired
    private CidadeRepository cidadeRepository;

    public void saveCidade(Cidade cidade) {

        cidadeRepository.save(cidade);
    }
}
