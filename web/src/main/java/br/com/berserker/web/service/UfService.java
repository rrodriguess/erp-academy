package br.com.berserker.web.service;

import br.com.berserker.core.entities.Uf;
import br.com.berserker.core.repository.UfRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UfService {

    @Autowired
    private UfRepository ufRepository;

    public void saveUf(Uf uf) {

        ufRepository.save(uf);
    }
}
