package br.com.berserker.web.controller;

import br.com.berserker.core.builders.AlunoBuilder;
import br.com.berserker.core.builders.EnderecoBuilder;
import br.com.berserker.core.entities.Aluno;
import br.com.berserker.core.entities.Cidade;
import br.com.berserker.core.entities.Endereco;
import br.com.berserker.core.entities.Uf;
import br.com.berserker.web.service.AlunoService;
import br.com.berserker.web.service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

@RestController
public class AlunoController {

    @Autowired
    private AlunoService alunoService;

    @Autowired
    private EnderecoService enderecoService;

    @GetMapping("/aluno")
    public void saveAluno(){
        System.out.println("Aluno chamado !!!");

        EnderecoBuilder bulderEndereco = new EnderecoBuilder();
        bulderEndereco.whithTipoLogradouro("Rua")
                       .whithLogradouro("Felix da Cunha")
                       .whithNumero("15")
                       .whithBairro("Parque São Rafael")
                       .whithComplemento("A")
                       .whithCep("08320-470")
                       .whithCidade(new Cidade("São Paulo", new Uf("SP")))
                       .whithIbge(0);
        Endereco endereco = bulderEndereco.build();

        enderecoService.saveEndereco(endereco);

        AlunoBuilder builder = new AlunoBuilder();
        builder.withCpf("22703395841")
                .withIdade(33)
                .withNascimento(Calendar.getInstance())
                .withNome("Renato")
                .withRg("321507903");
        Aluno aluno = builder.build();

        aluno.setEndereco(endereco);

        alunoService.saveAluno(aluno);

    }

    @GetMapping("/aluno/update")
    public Aluno updateAluno() {
        EnderecoBuilder bulderEndereco = new EnderecoBuilder();
        bulderEndereco.whithTipoLogradouro("Rua")
                .whithLogradouro(null)
                .whithNumero(null)
                .whithBairro("Parque São Rafael")
                .whithComplemento("A")
                .whithCep("08320-470")
                .whithCidade(new Cidade("São Paulo", new Uf("SP")))
                .whithIbge(0);

        Endereco endereco = bulderEndereco.build();

        enderecoService.updateEndereco(endereco);

        return null;

    }
}
