package br.com.berserker.web.service;

import br.com.berserker.core.entities.Cidade;
import br.com.berserker.core.entities.Endereco;
import br.com.berserker.core.entities.Uf;
import br.com.berserker.core.repository.EnderecoRepository;
import br.com.berserker.core.utils.ErrorMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.util.StringUtils.isEmpty;

@Service
public class EnderecoService {

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private UfService ufService;

    @Autowired
    private CidadeService cidadeService;

    public void saveEndereco(Endereco endereco) {

        Uf uf = new Uf(endereco.getUf().getSigla());
        ufService.saveUf(uf);

        Cidade cidade = new Cidade(endereco.getCidade().getNome(), uf);
        cidadeService.saveCidade(cidade);

        endereco.setCidade(cidade);

        enderecoRepository.save(endereco);
    }

    public void updateEndereco(Endereco endereco) {

        Collection<ErrorMessages> erros = EntityProperties.validate(endereco);

        for (ErrorMessages error : erros) {
            System.out.println(error.toString());
        }
//        Endereco newEndereco = enderecoRepository.findOne(endereco.getId());


    }

    private static class EntityProperties {
        public static Collection<ErrorMessages> validate(Endereco endereco) {
            Collection<ErrorMessages> errors = new ArrayList<>();
            Field[] fields = endereco.getClass().getDeclaredFields();

            for (Field field : fields) {
                field.setAccessible(true);
                if (field.getType().equals(String.class)) {

                    try {
                        String value = (String) field.get(endereco);
                        if (isEmpty(value)) {
                            errors.add(new ErrorMessages(field.getName() + " campo vázio ou nulo"));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            return errors;
        }
    }
}
