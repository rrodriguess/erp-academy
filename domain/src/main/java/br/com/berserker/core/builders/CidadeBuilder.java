package br.com.berserker.core.builders;

import br.com.berserker.core.entities.Uf;

/**
 * Created by renato on 20/06/17.
 */
public class CidadeBuilder {

    private String nome;

    private Uf uf;

    public CidadeBuilder whithNome (String nome) {
        this.nome = nome;
        return this;
    }

    public CidadeBuilder whithUf (String uf) {
        Uf uf1 = new Uf();
        uf1.setSigla(uf);
        this.uf = uf1;

        return this;
    }
}
