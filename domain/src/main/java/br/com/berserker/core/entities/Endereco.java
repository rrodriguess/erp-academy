package br.com.berserker.core.entities;

import javax.persistence.*;

@Entity
public class Endereco {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String logradouro;

    private String tipoLogradouro;

    private String  cep;

    private String numero;

    private String complemento;

    private String bairro;

    private Integer ibge;

    @OneToOne
    private Cidade cidade;

    @Deprecated
    public Endereco(){}

    public Endereco(String logradouro, String tipoLogradouro, String cep, String numero, String complemento, String bairro, Integer ibge, Cidade cidade) {
        this.logradouro = logradouro;
        this.tipoLogradouro = tipoLogradouro;
        this.cep = cep;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
        this.ibge = ibge;
        this.cidade = cidade;
    }

    public Long getId() {
        return id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public Integer getIbge() {
        return ibge;
    }

    public void setIbge(Integer ibge) {
        this.ibge = ibge;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Uf getUf() {
        return this.cidade.getUf();
    }

    public void setUf(Uf uf) {
        this.cidade.setUf(uf);
    }

}
