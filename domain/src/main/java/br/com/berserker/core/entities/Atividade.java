package br.com.berserker.core.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Atividade {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @OneToOne
    private Dia dia;

    public Long getId() {
        return id;
    }

    public Dia getDia() {
        return dia;
    }

    public void setDia(Dia dia) {
        this.dia = dia;
    }
}
