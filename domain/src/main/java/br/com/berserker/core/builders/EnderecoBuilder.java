package br.com.berserker.core.builders;

import br.com.berserker.core.entities.Cidade;
import br.com.berserker.core.entities.Endereco;

/**
 * Created by renato on 20/06/17.
 */
public class EnderecoBuilder {

    private String logradouro;

    private String tipoLogradouro;

    private String  cep;

    private String numero;

    private String complemento;

    private String bairro;

    private Integer ibge;

    private Cidade cidade;

    public EnderecoBuilder whithLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public EnderecoBuilder whithTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
        return this;
    }

    public EnderecoBuilder whithCep(String cep) {
        this.cep = cep;
        return this;
    }

    public EnderecoBuilder whithNumero(String numero) {
        this.numero = numero;
        return this;
    }

    public EnderecoBuilder whithComplemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public EnderecoBuilder whithBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public EnderecoBuilder whithIbge(Integer ibge) {
        this.ibge = ibge;
        return this;
    }

    public EnderecoBuilder whithCidade(Cidade cidade) {
        this.cidade = cidade;
        return this;
    }

    public Endereco build() {
        return new Endereco(logradouro,tipoLogradouro,cep,numero,complemento,bairro,ibge,cidade);
    }
}
