package br.com.berserker.core.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Uf {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String sigla;

    public Uf(){}

    public Uf(String sigla) {
        this.sigla = sigla;
    }

    public Long getId() {
        return id;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
