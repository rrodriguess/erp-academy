package br.com.berserker.core.entities;

import com.sun.istack.internal.NotNull;

import javax.persistence.*;

@Entity
public class Cidade {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String nome;

    @OneToOne
    private Uf uf;

    @Deprecated
    public Cidade() {}

    public Cidade(String nome, Uf uf) {
        this.nome = nome;
        this.uf = uf;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Uf getUf() {
        return uf;
    }

    public void setUf(Uf uf) {
        this.uf = uf;
    }
}
