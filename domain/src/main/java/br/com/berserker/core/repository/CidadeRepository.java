package br.com.berserker.core.repository;

import br.com.berserker.core.entities.Cidade;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by renato on 21/06/17.
 */
public interface CidadeRepository extends JpaRepository<Cidade, Long> {
}
