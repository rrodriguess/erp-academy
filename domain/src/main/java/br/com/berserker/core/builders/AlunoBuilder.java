package br.com.berserker.core.builders;

import br.com.berserker.core.entities.Aluno;
import br.com.berserker.core.entities.Endereco;

import java.util.Calendar;

/**
 * Created by renato on 20/06/17.
 */
public class AlunoBuilder {

    private String nome;

    private Integer idade;

    private Calendar nascimento;

    private String rg;

    private String cpf;

    private Endereco endereco;

    public AlunoBuilder withNome(String nome) {
        this.nome = nome;
        return this;
    }

    public AlunoBuilder withIdade(Integer idade) {
        this.idade = idade;
        return this;
    }

    public AlunoBuilder withNascimento(Calendar nascimento) {
        this.nascimento = nascimento;
        return this;
    }

    public AlunoBuilder withRg(String rg) {
        this.rg = rg;
        return this;
    }

    public AlunoBuilder withCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public AlunoBuilder whithEndereco(Endereco endereco) {
        this.endereco = endereco;
        return  this;
    }

    public Aluno build() {
        return new Aluno(nome,idade,nascimento,rg,cpf,endereco);
    }
}
