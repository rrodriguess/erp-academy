package br.com.berserker.core.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by renato on 14/06/17.
 */
public class DateUtils {

    public static Calendar formatCalendar(Calendar data) {
        Calendar  calendar = new GregorianCalendar(data.YEAR, data.MONTH, data.DAY_OF_MONTH);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        return calendar;
    }

}
